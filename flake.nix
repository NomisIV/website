{
  description = "A flake for building my website";

  inputs = {
    nixpkgs.url = "nixpkgs";
    pandoc-tree-sitter-filter.url = "git+https://codeberg.org/NomisIV/pandoc-tree-sitter-filter";
    diosevka.url = "git+https://codeberg.org/NomisIV/diosevka";
  };

  outputs = {
    self,
    nixpkgs,
    diosevka,
    pandoc-tree-sitter-filter,
  } @ inputs: let
    systems = ["x86_64-linux" "aarch64-linux"];

    config = sys: let
      pkgs = import nixpkgs {system = sys;};

      haskellWithPackages = pkgs.haskellPackages.ghcWithPackages (
        pkgs:
          with pkgs; [
            pandoc
            warp
            wai
            listsafe
            hsass
            svg-tree
            rasterific-svg
            JuicyPixels
            stringsearch
            optparse-applicative
            mime-types
            rss
            fsnotify
            extra
            uri-encode
            unordered-containers
            websockets
            wai-websockets
          ]
      );

      diosevkaPath = "${diosevka.packages.${sys}.woff2}/share/fonts/diosevka/woff2";

      pandoc-tree-sitter-filterPath = "${pandoc-tree-sitter-filter.packages.${sys}.default}/bin/pandoc-tree-sitter-filter";
    in {
      nixosModules.default = (import ./module.nix) inputs;

      packages.${sys} = rec {
        nomisiv-website = pkgs.stdenv.mkDerivation {
          name = "nomisiv-website";
          src = ./src;
          buildInputs = with pkgs; [haskellWithPackages makeWrapper];
          buildPhase = ''
            ghc -O Main -o nomisiv-website
          '';
          installPhase = ''
            install -D nomisiv-website $out/bin/nomisiv-website
            wrapProgram $out/bin/nomisiv-website \
              --set DIOSEVKA_PATH ${diosevkaPath} \
              --set PANDOC_TREE_SITTER_FILTER ${pandoc-tree-sitter-filterPath}
          '';
        };
        default = nomisiv-website;
      };

      devShells.${sys}.default = pkgs.mkShell {
        packages = with pkgs; [
          proselint
          nil
          vscode-langservers-extracted
          marksman
          haskellWithPackages
          haskell-language-server
          pandoc-tree-sitter-filter.packages.${sys}.default
          jq
        ];

        DIOSEVKA_PATH = diosevkaPath;
        PANDOC_TREE_SITTER_FILTER = pandoc-tree-sitter-filterPath;
      };

      formatter.${sys} = pkgs.alejandra;

      checks.${sys}.module = pkgs.nixosTest {
        name = "module test";

        nodes = {
          server = {pkgs, ...}: {
            imports = [self.nixosModules.default];
            environment.systemPackages = with pkgs; [curl];
            services.nomisiv-website.enable = true;
          };
        };

        testScript = ''
          server.wait_for_unit("nomisiv-website.service")
          server.wait_for_open_port(8000)
          server.succeed("curl localhost:8000")
        '';
      };
    };

    mergeSystems = acc: system:
      with nixpkgs.legacyPackages.${system};
        lib.attrsets.recursiveUpdate acc (config system);
  in
    builtins.foldl' mergeSystems {} systems;
}
