{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}

module Generator.Converters
  ( Converter,
    noConvert,
    svgToPng,
    mdToHtml,
    mdToMeta,
    scssToCss,
  )
where

import Codec.Picture qualified as Picture
import Control.Monad.Trans (lift)
import Control.Monad.Trans.Except (except, throwE, withExceptT)
import Data.ByteString (ByteString)
import Data.ByteString qualified as ByteString
import Data.Default (def)
import Data.Function ((&))
import Data.Functor ((<&>))
import Data.List.NonEmpty qualified as NEList
import Data.List.Safe qualified as List
import Data.Map qualified as Map
import Data.Maybe qualified as Maybe
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.Encoding (encodeUtf8)
import Data.Text.IO qualified as TextIO
import Generator.Error
  ( Error
      ( CouldNotParseSvg,
        PandocError,
        SassError,
        UndefinedEnvironmentVariable
      ),
    Result,
    note,
  )
import Graphics.Rasterific.Svg qualified as Rasterific
import Graphics.Svg qualified as Svg
import Graphics.Text.TrueType qualified as TrueType
import System.Environment (lookupEnv)
import Text.DocTemplates qualified as DocTemplates
import Text.Pandoc
  ( Extension (..),
    Meta,
    ObfuscationMethod (..),
    Pandoc (Pandoc),
    ReaderOptions (..),
    WithPartials (runWithPartials),
    WriterOptions (..),
  )
import Text.Pandoc qualified as Pandoc
import Text.Pandoc.Filter (Filter (JSONFilter), applyFilters)
import Text.Pandoc.Scripting (noEngine)
import Text.Sass qualified as Hsass

type Converter = String -> Result ByteString

noConvert :: Converter
noConvert inFile = lift $ ByteString.readFile inFile

svgToPng :: Converter
svgToPng svgFile = do
  svg <- lift $ ByteString.readFile svgFile

  (foo, _) <- case Svg.parseSvgFile svgFile svg of
    Just doc ->
      lift $ Rasterific.renderSvgDocument TrueType.emptyFontCache Nothing 96 doc
    Nothing -> throwE CouldNotParseSvg

  pure $
    foo
      & Picture.encodePng
      & ByteString.toStrict

mdToHtml :: Bool -> String -> String -> Converter
mdToHtml liveReload template htmlFile mdFile =
  do
    md <- lift $ TextIO.readFile mdFile

    templateContent <- lift $ TextIO.readFile template

    let envVar = "PANDOC_TREE_SITTER_FILTER"

    pandocTreeSitterFilter <-
      lift (lookupEnv envVar)
        >>= note (UndefinedEnvironmentVariable envVar)

    let pandocStuff = do
          templateE <-
            templateContent
              & Pandoc.compileTemplate template
              & runWithPartials

          let t = case templateE of
                Left _ -> Nothing
                Right t' -> Just t'

              extensions =
                Pandoc.pandocExtensions
                  & Pandoc.enableExtension Ext_auto_identifiers
                  & Pandoc.enableExtension Ext_autolink_bare_uris
                  & Pandoc.enableExtension Ext_emoji
                  & Pandoc.disableExtension Ext_implicit_figures
                  & Pandoc.disableExtension Ext_markdown_in_html_blocks

              readOptions = def {readerExtensions = extensions}

              breadcrumbs :: Text
              breadcrumbs =
                let path = Text.splitOn "/" $ Text.pack htmlFile
                    path' = do
                      pathInit :: [Text] <- List.init path
                      pathLast <- List.last path
                      pathInit
                        ++ [ Text.stripSuffix ".html" pathLast
                               & Maybe.fromMaybe pathLast
                           ]

                    -- Returns a list of nonempty lists, like inits,
                    -- but without the initial empty list.
                    -- This assures that all the inner lists will never be empty
                    inits' :: [a] -> [NEList.NonEmpty a]
                    inits' list =
                      list
                        & List.inits
                        & List.drop 1
                        <&> NEList.fromList

                    pathLinks =
                      path'
                        & inits'
                        <&> ( \seg ->
                                case NEList.last seg of
                                  "index" -> ""
                                  text -> "<a href=\"/" <> href <> "\">" <> text <> "</a>"
                                    where
                                      href = Text.intercalate "/" $ NEList.toList seg
                            )
                        & ("<a href=\"/\">nomisiv.com</a>" :)
                        & Text.intercalate " / "
                 in pathLinks

              context =
                [ ("breadcrumbs", breadcrumbs),
                  ("lang", "en"),
                  ("dir", "ltr"),
                  ("url", "https://nomisiv.com/" <> Text.pack htmlFile),
                  ("liveReload", if liveReload then "yes" else "")
                ]
                  <&> (\(a, b :: Text) -> (a, DocTemplates.toVal b))
                  & Map.fromList
                  & DocTemplates.Context

              writeOptions =
                def
                  { writerTemplate = t,
                    writerVariables = context,
                    writerEmailObfuscation = ReferenceObfuscation,
                    writerTableOfContents = True
                  }
          ast <- Pandoc.readMarkdown readOptions md

          ast2 <- applyFilters noEngine def [JSONFilter pandocTreeSitterFilter] [] ast

          Pandoc.writeHtml5String writeOptions ast2

    Pandoc.runIO pandocStuff
      & lift
      >>= except
      & withExceptT PandocError
      <&> encodeUtf8

mdToMeta :: String -> Result Meta
mdToMeta mdFile =
  do
    file <- lift $ TextIO.readFile mdFile

    result <- lift $ Pandoc.runIO $ do
      (Pandoc meta _) <-
        Pandoc.readMarkdown
          def {readerExtensions = Pandoc.pandocExtensions}
          file
      pure meta

    except $ case result of
      Left err -> Left $ PandocError err
      Right res -> Right res

scssToCss :: Converter
scssToCss scssFile =
  do
    scss <- lift $ readFile scssFile
    y <- lift $ Hsass.compileString scss def

    case y of
      Left err -> throwE $ SassError err
      Right css -> pure $ encodeUtf8 $ Text.pack css
