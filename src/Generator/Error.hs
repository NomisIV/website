{-# OPTIONS_GHC -Wall #-}

module Generator.Error (Error (..), Result, note) where

import Control.Monad.Except (ExceptT)
import Control.Monad.Trans.Except (except)
import Data.Text qualified as Text
import Text.Pandoc qualified as Pandoc
import Text.Sass qualified as Sass

type Result a = ExceptT Error IO a

note :: e -> Maybe a -> ExceptT e IO a
note _ (Just a) = except $ Right a
note e Nothing = except $ Left e

data Error
  = PandocError Pandoc.PandocError
  | NoIndexInDir String
  | CouldNotParseSvg
  | UndefinedEnvironmentVariable String
  | SassError Sass.SassError

instance Show Error where
  show (PandocError t) = Text.unpack $ Pandoc.renderError t
  show (NoIndexInDir dir) = "Could not find an index.md in " ++ dir
  show CouldNotParseSvg = "Could not parse SVG document"
  show (UndefinedEnvironmentVariable v) = "Environment variable " <> v <> " is not set"
  show (SassError e) = show e
