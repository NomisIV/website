{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}

module Generator.Lib (indexGenerator, substitute, includeDir, Pages, Page) where

import Control.Monad (filterM)
import Control.Monad.Trans (lift)
import Control.Monad.Trans.Except (except)
import Data.ByteString (ByteString)
import Data.ByteString.Lazy qualified as ByteString
import Data.ByteString.Search qualified as ByteStringSearch
import Data.ByteString.UTF8 (fromString)
import Data.Function ((&))
import Data.Functor ((<&>))
import Data.HashMap.Strict (HashMap)
import Data.HashMap.Strict qualified as Map
import Data.List.Safe qualified as List
import Data.Maybe qualified as Maybe
import Data.Text qualified as Text
import Data.Text.Encoding (encodeUtf8)
import Data.Time (Day, UTCTime (..))
import Data.Time qualified as Time
import Generator.Converters
  ( mdToHtml,
    mdToMeta,
    noConvert,
  )
import Generator.Error (Error (NoIndexInDir), Result)
import Network.URI (URI (..), URIAuth (..))
import System.Directory (doesDirectoryExist, doesFileExist, listDirectory)
import System.Directory qualified as Directory
import System.FilePath (takeFileName)
import System.FilePath.Posix ((-<.>), (<.>), (</>))
import System.FilePath.Posix qualified as FilePath
import Text.Pandoc (Meta, docAuthors, docDate, docTitle)
import Text.Pandoc.Shared (stringify)
import Text.Pandoc.Writers.Shared (lookupMetaString)
import Text.RSS (ItemElem (..), RSS (..))
import Text.RSS qualified as RSS

type Pages = HashMap String ByteString

type Page = (String, ByteString)

substitute :: [(String, String)] -> ByteString -> ByteString
substitute substitutions bs =
  foldl
    ( \bss (from, to) ->
        ByteString.toStrict $
          ByteStringSearch.replace
            (encodeUtf8 $ Text.pack $ "{" <> from <> "}")
            (encodeUtf8 $ Text.pack to)
            bss
    )
    bs
    substitutions

rssXml :: String -> Meta -> [(String, Meta)] -> Result Page
rssXml dir indexMeta pagesMetaList =
  do
    let page = dir </> dir <.> "xml"
    content <-
      let uriAuth = URIAuth "" "nomisiv.com" ""

          uri = URI "https:" (Just uriAuth) ("/" <> dir) "" ""

          title = Text.unpack $ stringify $ docTitle indexMeta

          description =
            Text.unpack $
              lookupMetaString "description" indexMeta

          mkEntry (filePath, meta) =
            let dateMaybe :: Maybe Day =
                  docDate meta
                    & stringify
                    & Text.unpack
                    & Time.parseTimeM
                      False
                      Time.defaultTimeLocale
                      "%Y-%-m-%-d"
             in [ Title $ Text.unpack $ stringify $ docTitle meta,
                  Guid True $
                    "https://nomisiv.com" </> filePath -<.> "html",
                  Author $
                    Text.unpack $
                      Text.concat $
                        stringify <$> docAuthors meta
                ]
                  ++ ( dateMaybe
                         <&> PubDate
                           . ( \day ->
                                 UTCTime day $
                                   Time.secondsToDiffTime 0
                             )
                         & Maybe.maybeToList
                     )

          rss = RSS title uri description [] $ pagesMetaList <&> mkEntry
       in rss
            & RSS.rssToXML
            & RSS.showXML
            & fromString
            & pure
    pure (page, content)

includeDir :: FilePath -> FilePath -> Result Pages
includeDir path location =
  do
    dirContents <- listDirectory path & lift <&> map (path </>)
    dirs <-
      filterM doesDirectoryExist dirContents
        & lift
        >>= mapM (\p -> includeDir p (location </> takeFileName p))
        <&> Map.unions
    files <-
      filterM doesFileExist dirContents
        & lift
        >>= mapM (\f -> noConvert f <&> (location </> takeFileName f,))
        <&> Map.fromList
    pure $ dirs <> files

indexGenerator ::
  Bool ->
  String ->
  ([(FilePath, Meta)] -> [String]) ->
  Bool ->
  Result Pages
indexGenerator liveReload dir formatter generateRss =
  do
    files <- lift $ Directory.listDirectory dir <&> map (dir </>)

    -- TODO: Use List.partition instead?
    indexPath <-
      files
        & List.find (\f -> FilePath.takeFileName f == "index.md")
        & \case
          Just indexPath -> Right indexPath
          Nothing -> Left $ NoIndexInDir dir
        & except

    indexMeta <- mdToMeta indexPath

    let subPages =
          files
            & List.filter (\f -> FilePath.takeFileName f /= "index.md")

    subPageMetas <-
      subPages
        & mapM
          ( \fileName -> do
              meta <- mdToMeta fileName
              pure (fileName, meta)
          )

    index <-
      do
        let list =
              subPageMetas
                & formatter
                & map (\x -> "<li>" <> x <> "</li>")
                & List.concat
                & (\x -> "<ul>" <> x <> "</ul>")

        html <-
          mdToHtml
            liveReload
            "template.html"
            (indexPath -<.> "html")
            indexPath

        let content = html & substitute [(dir, list)]
        pure (indexPath -<.> "html", content)

    rss <- rssXml dir indexMeta subPageMetas

    subPages' <-
      subPages
        & mapM
          ( \subPage ->
              do
                content <-
                  mdToHtml
                    liveReload
                    "template.html"
                    (subPage -<.> "html")
                    subPage
                pure (subPage -<.> "html", content)
          )

    [index] ++ [rss | generateRss] ++ subPages'
      & Map.fromList
      & pure
