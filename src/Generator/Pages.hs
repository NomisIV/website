{-# OPTIONS_GHC -Wall #-}

module Generator.Pages (pages, Pages, Page) where

import Control.Monad.Except (ExceptT (..))
import Data.Either.Extra (maybeToEither)
import Data.Function ((&))
import Data.Functor ((<&>))
import Data.HashMap.Strict qualified as Map
import Data.List.Safe qualified as List
import Data.Text qualified as Text
import Generator.Converters
  ( mdToHtml,
    noConvert,
    scssToCss,
    svgToPng,
  )
import Generator.Error (Error (UndefinedEnvironmentVariable), Result)
import Generator.Lib (Page, Pages, includeDir, indexGenerator)
import System.Environment (lookupEnv)
import System.FilePath.Posix ((<.>))
import System.FilePath.Posix qualified as FilePath
import Text.Pandoc (docDate, docTitle)
import Text.Pandoc.Shared (stringify)

blog :: Bool -> Result Pages
blog liveReload =
  indexGenerator
    liveReload
    "blog"
    ( \metaList ->
        metaList
          & List.sortOn (\(_, meta) -> docDate meta)
          & List.reverse
          & List.map
            ( \(filePath, meta) ->
                let href = FilePath.takeBaseName filePath
                    date = docDate meta & stringify & Text.unpack
                    title = docTitle meta & stringify & Text.unpack
                 in date <> ": <a href=\"" <> href <> "\">" <> title <> "</a>"
            )
    )
    True

recipes :: Bool -> Result Pages
recipes liveReload =
  indexGenerator
    liveReload
    "recipes"
    ( List.map
        ( \(filePath, meta) ->
            let href = FilePath.takeBaseName filePath
                title = docTitle meta & stringify & Text.unpack
             in "<a href=\"" <> href <> "\">" <> title <> "</a>"
        )
    )
    False

assets :: Result Pages
assets =
  do
    extraAssets <- includeDir "assets" "assets"
    diosevka <- do
      let variants = ["Regular", "Bold", "Italic", "BoldItalic"]
          envVar = "DIOSEVKA_PATH"
      diosevkaPath <- lookupEnv envVar <&> maybeToEither (UndefinedEnvironmentVariable envVar) & ExceptT
      mapM
        ( \variant ->
            let path = "assets/fonts/Diosevka-" <> variant <.> "woff2"
             in noConvert (diosevkaPath <> "/Diosevka-" <> variant <.> "woff2") <&> (path,)
        )
        variants
        <&> Map.fromList
    pure $ extraAssets <> diosevka

pageList :: Bool -> Result Pages
pageList liveReload =
  do
    [ ("index.html", mdToHtml liveReload "template.html" "index.html" "index.md"),
      ("contact.html", mdToHtml liveReload "template.html" "contact.html" "contact.md"),
      ("lists.html", mdToHtml liveReload "template.html" "lists.html" "lists.md"),
      ("projects.html", mdToHtml liveReload "template.html" "projects.html" "projects.md"),
      ("games.html", mdToHtml liveReload "template.html" "games.html" "games.md"),
      ("wishlist.html", mdToHtml liveReload "template.html" "wishlist.html" "wishlist.md"),
      ("style.css", scssToCss "style.scss"),
      ("favicon.png", svgToPng "favicon.svg"),
      ("robots.txt", noConvert "robots.txt"),
      ("404.html", mdToHtml liveReload "template-error.html" "404.html" "errors/404.md")
      ]
      & mapM (\(a, b) -> b <&> (a,))
      <&> Map.fromList

minotaurHunter :: Result Pages
minotaurHunter = includeDir "../minotaur-hunter" "games/minotaur-hunter"

pages :: Bool -> Result Pages
pages liveReload =
  [pageList liveReload, blog liveReload, recipes liveReload, assets, minotaurHunter]
    & sequence
    <&> Map.unions
