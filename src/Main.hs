{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}

import Control.Concurrent.STM as STM
import Control.Concurrent.STM.TChan qualified as TChan
import Control.Monad (when)
import Control.Monad.Except (runExceptT)
import Data.ByteString (ByteString)
import Data.ByteString.Builder (byteString)
import Data.ByteString.UTF8 (fromString, toString)
import Data.Function ((&))
import Data.Functor (void, (<&>))
import Data.HashMap.Strict qualified as Map
import Data.List.Safe qualified as List
import Data.Maybe qualified as Maybe
import Data.Text qualified as Text
import Data.Time qualified as Time
import Generator.Pages (Pages, pages)
import Network.HTTP.Types
  ( Status (statusCode),
    status200,
    status301,
    status404,
    status405,
  )
import Network.Mime qualified as Mime
import Network.URI.Encode qualified as URI
import Network.Wai
  ( Application,
    Request (rawPathInfo, requestMethod),
    Response,
    responseBuilder,
  )
import Network.Wai.Handler.Warp qualified as Warp
import Network.Wai.Handler.WebSockets qualified as WebSockets
import Network.WebSockets qualified as WebSockets
import Options.Applicative
  ( ParserInfo,
    auto,
    execParser,
    fullDesc,
    header,
    help,
    helper,
    info,
    long,
    metavar,
    option,
    progDesc,
    short,
    showDefault,
    strOption,
    switch,
    value,
    (<**>),
  )
import System.Directory (setCurrentDirectory)
import System.Exit (exitFailure)
import System.FSNotify qualified as FSNotify
import System.FilePath.Posix ((<.>), (</>))
import System.FilePath.Posix qualified as FilePath
import System.IO (BufferMode (LineBuffering), hSetBuffering, stdout)
import System.Posix qualified as Posix

app :: TVar Pages -> Bool -> Application
app pageMapVar liveReload req respond =
  do
    p <- readTVarIO pageMapVar
    let stringToResponse :: Status -> FilePath -> ByteString -> Response
        stringToResponse s filePath content =
          responseBuilder
            s
            [("Content-Type", Mime.defaultMimeLookup $ Text.pack filePath)]
            $ byteString content

        path :: FilePath
        path = req & rawPathInfo & toString & URI.decode

        status :: Status -> Response
        status s =
          let fileName = show (statusCode s) <.> "html"
           in Map.lookup fileName p & Maybe.maybe (responseBuilder s [] mempty) (stringToResponse s fileName)

        get :: String -> Response
        get path' = case Map.lookup path' p of
          Just content -> stringToResponse status200 path' content
          Nothing -> status status404

        redirect :: String -> Response
        redirect path' =
          responseBuilder
            status301
            [("Location", fromString path')]
            mempty

        isDir = FilePath.hasTrailingPathSeparator path
        hasPage page = Map.member page p

        pathWithIndexHtml = FilePath.dropDrive $ path </> "index.html"
        pathWithoutTrailingSlash =
          FilePath.dropDrive $
            FilePath.dropTrailingPathSeparator path
        pathWithTrailingSlash =
          FilePath.dropDrive $
            FilePath.addTrailingPathSeparator path
        pathWithHtml = FilePath.dropDrive $ path <.> "html"

    respond =<< case requestMethod req of
      "GET" | liveReload && path == "/reload" -> do
        Posix.raiseSignal Posix.sigUSR1
        pure $ status status200
      "GET" | isDir && hasPage pathWithIndexHtml -> pure $ get pathWithIndexHtml
      "GET" | isDir && hasPage pathWithoutTrailingSlash -> pure $ redirect pathWithoutTrailingSlash
      "GET" | isDir -> pure $ status status404
      "GET" | hasPage pathWithIndexHtml -> pure $ redirect pathWithTrailingSlash
      "GET" | hasPage (FilePath.dropDrive path) -> pure $ get (FilePath.dropDrive path)
      "GET" | hasPage pathWithHtml -> pure $ get pathWithHtml
      "GET" -> pure $ status status404
      _ -> pure $ status status405

wsApp :: TChan.TChan () -> WebSockets.ServerApp
wsApp reloadC pendingConnection = do
  c <- WebSockets.acceptRequest pendingConnection
  WebSockets.withPingThread c 15 (pure ()) (STM.atomically $ TChan.readTChan reloadC)

data Opt = Opt
  { port :: Int,
    dir :: FilePath,
    liveReload :: Bool
  }

opt :: ParserInfo Opt
opt =
  info
    ( ( Opt
          <$> option
            auto
            ( short 'p'
                <> long "port"
                <> metavar "PORT"
                <> help "The port to bind the server to"
                <> showDefault
                <> value 8000
            )
          <*> strOption
            ( short 'd'
                <> long "directory"
                <> metavar "DIR"
                <> help "What directory to load the source files from"
                <> showDefault
                <> value "web"
            )
          <*> switch
            ( short 'r'
                <> long "live-reload"
                <> help "Enable live reloading (trigger on GET /reload or SIGUSR1)"
            )
      )
        <**> helper
    )
    ( fullDesc
        <> progDesc "Serve the nomisiv website"
        <> header "nomisiv-website"
    )

-- TODO: Check for broken links in pages
main :: IO ()
main = FSNotify.withManager $ \mgr -> do
  (Opt {port, dir, liveReload}) <- execParser opt

  hSetBuffering stdout LineBuffering

  setCurrentDirectory dir

  runExceptT (pages liveReload)
    >>= either
      (\e -> print e >> exitFailure)
      ( \p ->
          do
            pageMapVar <- STM.newTVarIO p
            reloadC <- STM.atomically TChan.newTChan

            let predicate (FSNotify.Modified eventPath _ FSNotify.IsFile)
                  | FilePath.takeExtension eventPath == ".md" = True
                  | FilePath.takeExtension eventPath == ".scss" = True
                predicate _ = False

                action (FSNotify.Modified _eventPath _ FSNotify.IsFile) = do
                  putStrLn "Reloading pages"
                  runExceptT (pages liveReload)
                    >>= either print (atomically . writeTVar pageMapVar)
                    & void
                  STM.atomically $ TChan.writeTChan reloadC ()
                  putStrLn "Reloading pages done"
                action _ = mempty

            when liveReload $ void $ FSNotify.watchTree mgr "." predicate action

            let settings =
                  [ Warp.setPort port,
                    Warp.setBeforeMainLoop $
                      putStrLn $
                        "Listening on http://localhost:" ++ show port,
                    Warp.setLogger
                      ( \req status _ -> do
                          date <- Time.getCurrentTime
                          putStrLn $
                            unwords
                              [ show date,
                                show $ statusCode status,
                                toString $ requestMethod req,
                                toString $ rawPathInfo req
                              ]
                      )
                  ]

            putStrLn $ p & Map.toList <&> fst & unlines
            putStrLn $ "Loaded " <> show (List.length p) <> " files."

            app pageMapVar liveReload
              & WebSockets.websocketsOr WebSockets.defaultConnectionOptions (wsApp reloadC)
              & Warp.runSettings (foldl1 (.) settings Warp.defaultSettings)
      )
