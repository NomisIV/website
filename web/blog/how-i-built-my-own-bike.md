% How I Buit My Own Bike
% simon@nomisiv.com (Simon Gutgesell)
% 2024-06-30

![Red gravel bike witha green forst background. Autumn vibes.](/assets/images/loca-forest.jpg){width=90%}

# How I Built My Own Bike

I never thought this day would come. It is literally in the name of this blog: "No one asked". But the other day someone on the Fediverse asked to know more about how I built my own bike.

## Why would you even build your own bike?

Well, if my dream bike doesn't exist yet, I'll have no choice but to build it myself, right?
The criteria I had in mind were:

- being able to go basically anywhere without being underbiked
- being able to bike whenever

Aligning this with the current bike trends, a gravel bike was a no-brainer.
Besides, drop bars look rad, don't they?

## So what kind of bike are you building, really?

Well, a gravel bike is kind of the best of both worlds when it comes to MTB and road riding. It is 80% of a road bike, while still being 80% of an MTB. It does not excell on the road or on the trails, but it is very well suited for the happy middle ground of gravel roads.

But I'm not just building any gravel bike, no no.

### Belt drive

I'm fed up with bike chains. They are rusty and noisy and cumbersome. They require regular maintenance, and they still wear out in a couple of years due to road salt from the winter conditions I bike in. The alternative to bike chains are *belts*, which come from two manufacturers:

- [Gates Carbon Drive](https://www.gatescarbondrive.com/bike)
- [VEER](https://www.veercycle.com/)

Veer is the newest player in this field, and offer an interesting product. Their belts can be split, in order to get it into the rear triangle.

> So if you don't know: In order to remove a chain from a bike frame you have to split it up. Reversely, this means you need to split either the belt or the frame to install a bike belt. More on this later ;)

But I went with Gates, since I opted for a splittable frame instead.

### Internal gear hub

But while belts have their improvements, they are fundamentally incompatible with standard derailleurs. Instead, they have to be used with internal gear hubs. These, again, come from mostly two manufacturers:

- Rohloff: These hubs offer up to 18 gears, but also *start* at $1.000
- Shimano Alfine: These hubs come in an 8-speed and an 11-speed version, and start at a more reasonable price of around $250

Considering the more reasonable price of the Alfine hubs, an 11-speed alfine hub was quite a no-brainer.

### Frame

The last component in the bike component trinity is the bike frame itself.

My previous choice of going with a belt drive system restricts the bike frame possibilities by a lot. The problem is twofold:

- The belt needs to be inside the rear right triangle of the frame. This is most commonly achieved by having a break in the frame, where it can be separated. Not very many frames have this.
- The belt also needs to be properly tensioned, to prevent it from slipping off. For this the frame requires *sliding dropouts*, and careful selection of belt length.

sliding dropouts
: The position of the rear wheel axle can be adjusted forwards and backwards relative to the frame

> Actually, the problem with belt tension is mostly just BS. Belt tension can be solved with a derailleur-like mechanism, tensioneing the belt at the rear wheel. I know this because VEER offer it as a piece of a converision kit they sell for one very specific bike model, but even that seems to be unobtainium. It amazes me how this product is not available anywhere. But I digress...

There are a select few frames on the market that fullfill these requirements for belt support, and the two cheapest options I could find were the following:

- [Soma Wolverine v4.1 Type-B](https://www.somafab.com/archives/product/wolverine-frame-v-4-1-type-b-adventure-monstercross)
- [Loca's City Bike Frame](https://www.somafab.com/archives/product/wolverine-frame-v-4-1-type-b-adventure-monstercross)

While the Soma is made from steel (which is desirable by some bike enthusiasts for its softer ride characteristic), the Loca bike is not only more aesthetically pleasing (in my subjectively objective opinion), but it is also made locally in Poland. And Poland is a lot closer to Sweden than the USA.

## Okay but what about the other parts?

There are indeed still a lot of bike parts remaining for a functional bike. Let's get into the details!

- It should have a hub dynamo to power lights, enabling me to bike at night at basically no extra cost.
- It should have long fenders, allowing me to bike in rain and wet conditions withouth getting road filth on me.
- A suspended seatpost adds a lot of comfort, especially on bad city roads. (Later I also invested in a suspended stem, which was really worth it)

## What mistakes did you make?

Being the first bike I ever built from scratch, I was bound to make mistakes. Here are the ones I recall:

### Wheel size

The bike frame clears 700Cx45mm tyres, according to the manufacturer, this means I can fit those tyres *and fenders*. **No I coudn't.** I have just now rebuilt the wheels to be 650Bx40mm instead, to clear the fenders.

### Brakes

I wasn't completely sure what disc brake rotor size, starting with 160mm in the front and 140mm rotors in the rear. This was due to the absolute comlexity that is mounting disc brakes to frames. There are like three different standards, and there are adapters between all of them. In the end I went with flat mount calipers (the frame was also flat mount). This required an adapter plate in the front, but I could direct mount the caliper in the rear. However I had to buy special screws that would fit this, and also change to 160mm in the rear, with an external lockring (because otherwise the tool wouldn't fit)

### Installing the gear shifter cable the wrong way around

At the rear wheel there is a bolt thing bolted to the shifter cable. This is then locked into the rear hub to allow changing gears. I couldn't find how to do this properly in any instruction manual, so I did my best. But every now and then the gears would skip or make horrible metallic pops and bangs. This was until I realized that I had, in fact, locked the bolt thing *the wrong way around* into the hub, making the shifter cable run ever so slightly diagonal, which offset the gear selection. This was a dumb mistake on my part, but it couldn't have hurt shimano too much to include som graphic for that would it? They throw in a user manual for every language in the EU, but no mention of this...

### Frame design

LOCA, the manufacturer, asked me if I wanted any custom graphics painted on the frame, but I wasn't sure at the time. Now I regret not sneaking in my logo hidden somewhere like an easter egg. That would have been pretty cool I think.

## How much did it cost?

Don't ask me that \:)

~~like 30.000 SEK in total~~

## Was it worth it?

Yes \:)
