% No One Asked
# No One Asked

This is my blog!

![No One Asked](/assets/memes/no-one-asked.jpg){width=50%}

I'm making no promises to write new blog posts regularly.
I kinda just write them as I feel like it,
and sometimes I don't really feel like it.

Here are my current blog posts:

{blog}

If you want you can subscribe to my blog using [RSS](blog.xml)!
