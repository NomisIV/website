% Nerd Fonts Suck
% simon@nomisiv.com (Simon Gutgesell)
% 2022-09-18
# Nerd Fonts Suck

[Nerd Fonts](https://github.com/ryanoasis/nerd-fonts)
has slowly become a standard for icon fonts on linux systems.
If you've ever riced your [polybar](https://github.com/polybar/polybar)
and wanted to have the same sick icons as your favorite linux youtuber have,
you have probably realized you have to download a nerd font.
Furthermore, it's adopted in
[vim](https://github.com/ryanoasis/vim-devicons)
[plugins](https://github.com/kyazdani42/nvim-web-devicons),
and [exa](https://github.com/ogham/exa) using the `--icons` flag,
and probably other places too.

Nerd Fonts work by patching existing fonts with their font patcher,
and they kindly provide a set of pre-patched fonts on their
[GitHub release page](https://github.com/ryanoasis/nerd-fonts/releases).
They use a bunch of different icon sets,
like [Material Design Icons](https://materialdesignicons.com/),
[Font Awesome](https://fontawesome.com/),
and [Devicon](https://devicon.dev/),
to name the biggest ones.
Then they combine these fonts with a font to be patched,
and boom, you now have a Nerd Font.

## So why does it suck?

My first experience with Nerd Fonts was when I was when I wanted to
rice my status bar with some sick icons for things like the CPU usage,
temperature, and battery percentage.
Specifically, I wanted to have the battery icon show the state of the battery.
Fortunately, Nerd Fonts have a two different
battery series icons to chose from, `nf-fa-battery` and `nf-mdi-battery`:

![nerd-fonts-fa-battery](/assets/images/nerd-fonts-batteries.png){width=100%}

The obvious winner here would be the `nf-mdi-battery`-series,
as they have the most diverse collection.
Also, they chose to have their battery in a vertical orientation,
meaning it will take up less space in the status bar.

The problem arose when I got around to the `nf-mdi-battery-charging`
sub-series of the collection.
It would be kinda sick if I could show that the battery is charging *and*
what percentage it's charging at,
but with the Nerd Fonts icons all I get are the 20, 30, 40, 60, 80, 90 and 100
percent variants. Alas, the 10 and 50 percent are missing. -_-

### Reason 1: Outdated icon packs

This is not directly Nerd Fonts' own fault,
as the Material Design Icons icon set didn't include them either.
At least not in the version that Nerd Fonts used to patch with...

After some searching around I found the
[Material Design Icons Font repo](https://github.com/Templarian/MaterialDesign-Font),
which included a much more up-to-date version of the icon pack.
(The
[version](https://github.com/ryanoasis/nerd-fonts/blob/master/src/glyphs/materialdesignicons-webfont.ttf)
Nerd Fonts still use is around five years old at this point)
This newer version did indeed include the missing charging battery icons.
However, as to not break backwards compatibility
by reshuffling all the other glyphs,
at a much later code point.

Which leads me to the next reason...

### Reason 2: Icon structuring

In case you aren't familiar with unicode,
it is basically a list of almost any symbol imaginable.
To name a few:

- Basic Latin (ASCII)
- A lot of punctuation and letter modifiers
- Math symbols
- Different kinds of arrows
- Egyptian hieroglyphs
- Chess pieces and playing cards??

I hope you get the picture.

Naturally, unicode even has dedicated sections for "private use",
which is where most icon fonts choose to put their glyphs.
These are:

- Private Use Area (E000 - F8FF)
- Supplementary Private Use Area A (F0000 - FFFFD)
- Supplementary Private Use Area B (100000 - 10FFFD)

Nerd Fonts store ~~all~~ most of their glyphs in the first section.

So what's the problem?

Allow me to explain with some basic napkin math:

1. Nerd Fonts patch fonts with 4115 extra icons,
according to the front page of their website.
1. The Private Use Area has a size of 0xf8ff - 0xe000 = 6399 glyphs

That seems to fit doesn't it?
Yes, however, the icon layouting of Nerd Fonts could have been better.
In a way to future proof the standard,
they added extra space between each icon set
to add space for future icons to be added.
The problem is that this extra space makes it all not fit inside the section.
All Nerd Fonts have some icons inside the "CJK Compability Ideographs"-section.
While this isn't a big problem for anyone who doesn't speak "CJK"
(Chinese, Japanese, and Korean),
it's still something that strictly goes against the philosophy of unicode.
An iconic trespassing, if you will. ;)

My biggest issue with this is that they could (and should) have use any of the
latter "Supplementary Private Use Areas" instead,
as they are both 65.533 glyphs each,
more than enough to fit all of Nerd Fonts symbols (including spacing)
nine times. Each.

Another fun consequence of this is that some fonts place common font diacritics
(like ◌́ and ◌̂)
in the Private Use Area, to be imported in other characters.
So when Nerd Fonts replaces these diacritics,
the font will still import whatever is there,
and you'll end up with gems like this:

![The invasion of Nerd Font tomatoes](/assets/images/nerd-font-tomato.png)

### Reason 3: Font patching

![What if I told you, you don't need a patched font](/assets/memes/morpheus-font-meme.jpg){width=50%}

Why *do* you need a patched font?
Of course you want to see the icons in your status bar or terminal or whatever,
but why should the icons be located in your main font? No, think about it.

I think there is a major misconception when it comes to fonts under linux.
Just because you set a single font in your terminal configuration doesn't
mean it's the only font that will be used.
As long as the program you're using is relatively modern
(like, the-last-couple-decades modern)
it should be using font libraries which support font fallbacks.
This means that any character your primary font can't draw,
will be represented using another font
that does have a code point for that glyph.
Like when installing emojies on your system,
all you *actually* have to do is to make sure
the font is detected by your system.
(Run something like `fc-list | grep <font-name>`
if you wanna see if it's detected or not).
Imagine if you had to patch your fonts with emojies,
that would be outrageous, no?

So why doesn't Nerd Fonts just ship a patched font with only symbols?
This would allow users to use Nerd Fonts in combination with any font they want.
In fact, they shipped a file called
[Symbols-1000-em Nerd Font Complete.ttf](https://github.com/ryanoasis/nerd-fonts/blob/2.2.0-RC/src/glyphs/Symbols-1000-em%20Nerd%20Font%20Complete.ttf)
that included all the glyphs,
and for a year now I've been using it as a complement to my unpatched fonts
without problems.
But in the latest releases they have removed it for some reason.
¯\\_ (ツ)_/¯

## Nerd Fonts 3.0

When I discovered the discrepancies with Nerd Fonts I wrote about my
findings in a
[GitHub issue comment](https://github.com/ryanoasis/nerd-fonts/issues/279#issuecomment-636498424),
proposing "a major reshuffle" of the glyphs.
`ryanoasis`, the owner of the project, commented on that and understandably
stated that the reshuffle wouldn't happen until Nerd Fonts version 3.0.
And as I have not seen a roadmap to the next major version,
I don't have high hopes of that ever happening.

## Conclusion

My solution to this is downloading the icon sets manually instead.
Currently I'm using the following icon sets:

- [Material Design Icons](https://materialdesignicons.com/)
- [Devicon](https://devicon.dev/)

NOTE: Since this isn't a valid Nerd Font configuration,
you will have to configure what glyphs to use manually
if the program supports it.

Another conclusion to take away from this post:

> If you are designing a standard, please make sure it's a good standard.

## Further reading:

- https://github.com/ryanoasis/nerd-fonts/issues/670
- https://github.com/ryanoasis/nerd-fonts/issues/279