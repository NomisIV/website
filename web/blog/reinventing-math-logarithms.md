% Reinventing Math - Logarithms
% simon@nomisiv.com (Simon Gutgesell)
% 2022-11-26
**NOTE:** My use of ^ in this article represents *exponentiation*,
e.g. taking something "to the power of" something else.
Like this: `2 ^ 3 = 2³`.
I use this syntax because Unicode still doesn't include superscript letters,
and I'm too lazy to figure out how to use ^superscript^ inside `code tags`.
I'll save reinventing markdown for another day ;)

# Reinventing Math - Logarithms

Math is one of the most essential tools we have for communicating logic,
and coming from a neatly organized and structured computer science environment,
it's often daunting to see how organic and inconsistant math can be.
I think this is a shame,
because once I get past the sometimes awful syntax of math,
the logic behind it is really beautiful.
I believe that math would be a lot easier to teach if only the syntax was
cleaner and more consequent.
And thus, let's reinvent math and discuss the possibly most
misunderstood operator: *the logarithm*.

## Operator levels

First, I will explain where logarithms stand in relation to the other
binary operators,
as I believe this makes then a lot easier to think about.

It's common to think about multiplication as repeated addition,
and perhaps also exponentiation (e.g. taking "*a* to the power of *b*"),
as repeated multiplication.
I really like this,
because it clearly explains the relation between the binary operators.
Where this usually starts to fall apart is when teaching about their opposing
operators, like division and square roots.
Of course it doesn't make sense to describe division as repeated subtraction,
but that is because that's not what it is.
Instead, let's define division as the *inverse* of multiplication,
such that `a * b / b = a`, since the `* b / b`-part cancels itself.
That is also easy to understand:
division simply "undoes" a repeated addition,
and gives you the original number.

Using these relations,
we can define a table describing how the operators relate to each other.
The positive operater for each level in the table
is just a repetition of the operator in the preceding level.
And likewise, the negative operator for each level is just the opposite
to the positive operator, undoing whatever it does.

| Level | Positive       | Negative                           |
|:-----:| -------------- | ---------------------------------- |
| 1     | Addition       | Subtraction                        |
| 2     | Multiplication | Division                           |
| 3     | Exponentiation | Root extraction / Logarithmization |

> NomisIV: An insight to take away here is that there is nothing stopping us
> from using a fourth level of operators,
> which would be a shorthand for repeated exponentiation:
> `a ^ (a ^ a) = a ^^ 3`[^tetration].

One curios thing is why there are two negative operators on level three.
Why is that?

## Commutativity

Let me explain why by introducing a new concept:

commutativity
: A property of binary operators which allow changing the order of its terms
  without changing the result.
: Or simply put, the order of the numbers don't matter.

Applying this property to the operators,
we can see that addition **is** commutative,

since `10 + 2 = 12` and `2 + 10 = 12`.

Multiplication **is also** commutative,

since `10 * 2 = 20` and `2 * 10 = 20`.

Exponentiation, however, **is *not*** commutative,

since `10 ^ 2 = 100` and `2 ^ 10 = 1024`, which clearly aren't equal.

The first observation when applying this to the negative operators is that
they aren't commutative by themselves.
`10 - 2 != 2 - 10`, `10 / 2 != 2 / 10`, etc.
However that's not really relevant here.
I guess you could call them commutative in *undoing* their respective
positive operator, though, and that's more interesting:

`12 - 10 = 2` and `12 - 2 = 10`.

Likewise `20 / 10 = 2` and `20 / 2 = 10`.

This means that we can recover both the `10` and the `2` from the result,
using the same operator.
However, because the exponentiation isn't commutative,
it's reasonable that we need two negative operators in order
to undo the exponentiation in both ways for excracting both numbers.

So `log(100) = 2`, and `√100 = 10`. Both numbers extracted!

Isn't something missing though?
Like, what about `1024`?

Ah yeah. Let's get to that.

## The hidden numbers

If we check the square root and logarithm of `1024`,
we get the following:

`√1024 = 32`

`log(1024) ≈ 3.01`

Uh oh. Those are new numbers. Where did they come from?

To understand this it's important to understand what we're actually doing here,
and as I alluded to in the beginning,
this is one of my main problems with math: the syntax.

We aren't *just* taking the root of `1024`,
but we're actually taking the second - or the *square* - root of `1024`.
There is a hidden `2` in there that is rarerly mentioned.
The square root's big brother - the *cube* root - is often denoted as `³√`,
though. And in fact the `n`th root can be written as `ⁿ√`

And the situation is the same, if not worse, with logarithms.
Again, we aren't *just* taking the logarithm of `1024`,
we're actually taking the logarithm for *base `10`* of `1024`.

> NomisIV: Why `10`? Because calculating the `log` of something is basically
> just counting how many zeroes the number has, and if you multiply something
> with `10`, you obviously add another zero.

To use the logarithm for base `n`, you'd write `logₙ`,
and `ln` - the natural logarithm - is also a logarithm, but with the base `e`
(the natural number).

So if we instead take the `10`th root, or the logarithm for base `2` of `1024`,
guess what we'll get?

`¹⁰√1024 = 2`

`log₂(1024) = 10`

Yay, those are our numbers!

## A new syntax

To make logarithms less confusing,
I suggest always explicitly writing out the base you're doing the logarithm
for: `logₙ`,
and in the case of the natural logarithm, write `logₑ` for consistency.
I would also wish that there was a symbol for logarithms,
instead of using actual word as if it was a function.
How about `\`, such that `log(100) = 100 \ 10 = 2`
and `log₂(1024) = 1024 \ 2 = 10`?
That's much cleaner and explicit!

And while we're at it, could we change up exponentiation and root extraction too?
How about `~`, such that `√100 = 100 ~ 2 = 10` and `¹⁰√1024 = 1024 ~ 10 = 2`?
That is much easier to read and write!

> NomisIV: Note that I also took the liberty of swapping the positions of the
> **radicand** and the **degree**, and the **antilogarithm** and the **base**
> [^wikitionary-logarithm]
> such that the bigger number comes first,
> just like in subtraction and division.

In turn, this would make undoing exponentiation much easier:

`10 ^ 2 ~ 2 = 10`

`10 ^ 2 \ 10 = 2`

`2 ^ 10 ~ 10 = 2`

`2 ^ 10 \ 2 = 10`

The important thing to notice here is that `~` undoes the *exponent*,
and `\` undoes the *base*, and you simply get the other number back.
That's even intuitive, once you learn it!

With this new syntax I also propose a new way of saying them,
since

> the square root of ten to the power of two equals ten

requires reading the entire expression before prononcing it
(if you're using my new syntax for it),
and

> ten to the power of two but inside the square root of two equals ten

doesn't exactly roll off the tounge.
Instead, how about:

> ten to the power of two root two equals ten

Likewise for logarithms:

> ten to the power of two but inside the tenth logarithm equals two

contra

> ten to the power of two log ten equals two

I know I just pulled this syntax out of nowhere,
and in fact there are most likely people mad at me for the symbols I chose,
but I just can't help but feel like *any* other symbols like `\` and `~`
would make the third level negative operators much easier to understand,
as they fit in better with the syntax and look of the other operators.

> NomisIV: Math really shouldn't have to be this hard, man.

[^tetration]: https://en.wikipedia.org/wiki/Tetration
[^wikitionary-logarithm]: https://en.wiktionary.org/wiki/logarithm#See_also
