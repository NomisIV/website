% Sick ARM NAS
% simon@nomisiv.com (Simon Gutgesell)
% 2024-11-25

![FirendlyElec CM3588 NAS kit with plexiglass top and a Noctua fan](/assets/images/sick-nas.jpg){width=90%}

# Building a fully sick 4-slot M.2 ARM NAS

My old NAS/server was an old, but fantastically beautifully engineered HP Z800 workstation that I put four 1TB hard drives in. It was ridiculously powerful for what it was. It had 96GB of RAM, and I believe it might even be the fastest Z800 in the world too, because I had upgraded the CPUs to a pair of Intel Xeon X5670s that weren't officially supported by HP, but still had a fair chance of working just based on the chipset. Alas, all this computing power also came with some power draw and noise generation. And thus, my desire for a small, quiet and solid state NAS was born.

Something like a year ago I watched a youtube video from LTT showcasing the FriendlyElec CM3588:

<iframe src="https://www.youtube.com/embed/QsM6b5yix0U"></iframe>

And then I was sold. What was also sold (out, even) was the NAS-kit on FriendlyElec's website. When they were back in stock I quickly bought one.

## Where can I buy one?

You can buy one here: [FriendlyElec CM3588](https://www.friendlyelec.com/index.php?route=product/product&path=60&product_id=294), but there is also a "Plus"-version which supports up to 32GB RAM and 256GB of onboard eMMC flash storage here: [FriendlyElec CM3588 Plus](https://www.friendlyelec.com/index.php?route=product/product&path=60&product_id=299)

This computer will never max out any SSD read/write tests, but that's not really the point of it. It's more than fast enough to saturate a 1Gb/s network link though, which is all I care about.

It is possible to run this computer fanless, which makes for a completely solid state NAS. However, you cannot unlock maximum performance from the RK3588 without a fan.

There are also other alternatives worth looking into for ARM-based NASes:

- [Radxa ROCK 5 ITX](https://radxa.com/products/rock5/5itx/) - A rk3588-based mini-ITX formfactor motherboard, for more conventional NAS builds
- [Radxa Penta SATA HAT](https://radxa.com/products/accessories/penta-sata-hat/) - A 4-port SATA attachment for your Raspberry Pi

I think the rk3588 chip used be the CM3588 NAS is the fastest ARM-chip in this class. Yes, there is the Apple M-chips, and yes, there are the Snapdragon X-chips, but I consider these a different league.

## Storage

I use four WD Blue 2TB M.2 drives for storage, and I have configured the drives in raidz1 using ZFS. This means the parity data is spread out on all drives. I can lose any one of them, while still retaining all data. This yeilds a total usable space of ~6TB.

There are M.2 SSDs with way more capacity, but I didn't want to empty my entire wallet for this project, so I settled for 2TB per drive, which around the sweet-spot for price to capacity.

## Power

I didn't bother with the optional wall adapter that you can buy from friendlelec themselves, because it would require an adapter to work for the outlets here. Instead I found an adapter on [adapterexperten.se](https://www.adapterexperten.se/) (Sweden only).

If you too want to bring your own power adaptor, these are the specs you'll want to keep in mind:

  - The power specifications are 12V 2A
  - The barrel plug size is 5.5*2.1mm, with the positive in the center

## Custom parts

The plexiglass top is custom. There is no official case for the computer, but there are CAD-files for 3D-printing your own if you so wish.

The Noctua fan on the heatsink has a modified connector. The connector name is called ZH 1.5, and you will probably need to modify this connector on any fan you install, as I haven't found any fans with this connector out of the box. You can buy the [connector housing](https://www.electrokit.com/kontakthus-zh-2-pol-1.5mm), [pins](https://www.electrokit.com/crimphylsa-zh-26-28-awg), and the [adorable fan protector](https://www.electrokit.com/ventilationsgaller-40x40-mm) from elektrokit (in Sweden at least).

If I'd make another one of these I'd probably skip the fan honestly. The performance difference is not something I've noticed, and while it does run a lot cooler, it still ran pretty alright without the fan (although, mostly just idling). Something to keep in mind: you probably don't need a fan - it will make it 100% less solid state.

## Operating System

I run this NAS with NixOS (like all of my other machines). It was quite the mission to get it installed, and I had to flash [edk2-rk3588](https://github.com/edk2-porting/edk2-rk3588) for an UEFI environment in the process.


This board recieved mainline linux support at kernel version 6.11, [but there are still a bunch of features missing](https://gitlab.collabora.com/hardware-enablement/rockchip-3588/notes-for-rockchip-3588/-/blob/main/mainline-status.md), most notably HDMI output.

FriendlyElec offers a [bunch of OSes](https://wiki.friendlyelec.com/wiki/index.php/CM3588#Install_OS) that Just Work™ out of the box. I've also tried [armbian to work on this board](https://www.armbian.com/nanopc-cm3588-nas/), which worked great.

### Conclusion

Yes, it's worth it and you should build one too.

![Do it. Do it Now.](/assets/memes/davie504-do-it-now.jpg){width=90%}
