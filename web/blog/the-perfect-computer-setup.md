% The Perfect Computer Setup
% simon@nomisiv.com (Simon Gutgesell)
% 2022-11-15
# The Perfect Computer Setup

*(Or, "The Onion Computer Setup")*

When programming in an enterprise environment there are certain tools and
paradigms that are commonly followed.
In particular,
I'm thinking of everyone's favorite, compiled, cross-platform, multi-paradigm,
and statically typed programming language, Java,
and it's innovative and bold introduction of object-level abstraction.
Isn't it wonderful to not have to worry about how something is implemented?

However,
something that is decidedly not abstract enough is the hardware beneath the
JVM, and I think it can be significantly improved by adding more abstraction.
So allow me to explore how this can fixed.

The first and possibly most obvious abstraction would be powering the computer
through a UPS (1), which will add a level of abstraction to the power grid.
No longer do I have to worry about if there are any power outages,
or what the power specifications of my outlets are.
69Hz and 420W? Sounds good to me!

For the hard drives I don't want to just let my computer interface them
directly, since that would become problematic if I ever wanted to switch out
the hard drives without losing the data on them.
Instead I'm going to use RAID to combine multiple hard drives into a single
virtual device that I can then create my partitions on.
I like this, because it adds another layer (2),
abstracting the hard drives away from the computer.
Furthermore,
I will be adding labels (like `boot`, `root` and `home`)
to the partitions on the virtual device,
which will allow me to quickly swap out the partition for another one
by just changing the partition labels (3).

Once the hard drives are prepared,
I wouldn't want to put them inside the computer itself
as that would restrict my choice of computer.
Maybe I want a computer with a really fast processor for compiling code
one day, but the next day I wanna game at 4k at 120Hz,
or maybe I just feel like having a slow day on an old Pentium D.
Instead,
by connecting the hard drives to the server in my closet
and accessing them over the network,
I can use whatever computer I want! (4)

"But how are you gonna boot your computer
if the hard drives aren't even inside the computer"
you might ponder.
Worry not; I already have an answer for that.
I will be using PXE to remotely boot the boot partition over the network (5),
then I will configure GRUB to attach the root partition as an iSCSI disk (6),
and later on in the boot process
the home partition (and any other partitions I might want)
will be mounted using NFS (7).
See, that's how.

Once the machine has completed booting without as much as a byte of storage
inside it,
there will be a VM set up where I'd do the actual work (8),
as using the system directly would indeed be too un-abstracted.
But dealing with system configuration files on the VM would be a huge pain
that I'd like to have hidden under another layer,
so naturally NixOS will be installed on the VM (9).
This will let me declaratively define my system configuration,
so that I don't have to configure it myself.
And the first thing I would configure on the NixOS VM is a VPN
in order to abstract away the local network (10).

The next step in the abstraction stair case is not logging in on the VM
directly.
I wouldn't want to limit myself to what computer setup I would want to use,
would I?
So instead I will use VNC to remotely connect to the VM over the network
using a thin client (11).
This way I wouldn't have to interface the computer directly.

Once connected and logged in to the VM I wouldn't want to limit myself to
nixpkgs 80.000+ packages,
and also because I shouldn't put all my eggs in the same basket.
Instead I will use Distrobox (12) so that I can install packages for any
linux distribution out there using containers.
Then I will download the JVM (13) for whatever distro I feel like,
to add the first *absolutely necessary* layer of abstraction
(since all of my custom scripts and programs are written in Java already).
And for developing more programs I will also be using Maven (14)
as a build system,
but inside of a nix-shell (15) for another two layers of abstraction.

Oh, and for starting the computer I will of course be using Wake On LAN (16),
for a total of 16 layers of abstraction.
I think this is a significant improvement over the normal hard ware setups! :)

---

**Disclaimer:** I don't *actually* recommend this,
it's just a silly thought eperiment.

**Disclaimer:** I haven't tried doing all of this either,
so I might have gotten some things wrong.
However, I'm quite sure that it's technically possible to set up,
and that's what I think makes it funny.

[This Arch Wiki artice](https://wiki.archlinux.org/title/ISCSI/Boot)
gave me most of the inspiration for this post.
