% Contact
# Contact

I usually go by the name "NomisIV" online.

- **E-mail:** [simon@nomisiv.com](mailto:simon@nomisiv.com)
- **Discord:** nomisiv
- **Matrix:** @nomisiv:matrix.org
- **Mastodon:** [@nomisiv@c.im](https://c.im/web/@nomisiv)
- **GitHub:** [NomisIV](https://github.com/NomisIV)
- **Codeberg:** [NomisIV](https://codeberg.org/NomisIV)
- **Bandcamp:** [nomisiv](https://bandcamp.com/nomisiv)
