---
author: Simon Gutgesell
title: Resume - Simon Gutgesell
---

# Simon Gutgesell

|               |                                               |
| -------------:|:--------------------------------------------- |
| **Age**       | {age} years                                   |
| **Phone**     | +4672-40 60 115                               |
| **E-mail**    | [simon@nomisiv.com](mailto:simon@nomisiv.com) |
| **Address**   | Skogslyckegatan 86 Linköping, Sweden          |
| **Languages** | Swedish (native), English (fluent)            |

## Summary

Do you value efficiency, reliability and quality? So do I!
I'm an ambitious and computer-interested young man,
with an eye for perfecting the details.
I value open-source, and I use linux on all of my computers.
The programming languages I'm most proficient in are
PureScript, Haskell, Rust and Nix,
but I'm also familliar with Python, JavaScript / TypeScript and Java.
In my spare time I spend a lot of time working on personal coding projects,
like my own website.

Another interest of mine is biking,
because it gets me out into nature and it makes me feel alive.
I have four bikes, one of which I built myself from bike scraps I got from my dad.

## Work Experience

### \[2023-XXXX\]: Full time developer at Lesslie

### \[2022-2023\]: Sysadmin / IT-support in a Microsoft environment at SundaHus

Common tasks include setting up new user accounts and computers,
and acting as IT-support for internal problems.
I also installed and configured the monitoring of their servers,
their backup server, and their new UPS.

## Education

- **Civil Engineering in Software and Technology** at LiU. *(Unfinished)*
- **The Technology Programme** in Ullvigymnasiet, Köping.

## Projects

### \[2023,2024\] LiTHe Kod Webmaster

LiTHe kod is a student organization at Linköpings universitet
whose purpose is to promote and inspire interest for programming,
software development and everything that goes with it.
I was responsible for the LiTHe Kod website during this period.

### \[2022\] My Website

Developing my own website has always been something I've wanted to do.
So I made one, completely from scratch.

### \[2021\] D-LAN Web & Stream

D-LAN is a project group at LiU which arranges a LAN night every year at campus.
In the school year 2020 - 2021 I maintained the project group's website.

### \[2020\] Kylbrant's Photos

I developed a gallery store website for Sandra Kylbrant's photography business.
