---
author: Simon Gutgsesell
title: CV - Simon Gutgesell
---

# Simon Gutgesell

|             |                                               |
| -----------:|:--------------------------------------------- |
| **Ålder**   | {age} år                                      |
| **Telefon** | 072-40 60 115                                 |
| **E-mail**  | [simon@nomisiv.com](mailto:simon@nomisiv.com) |
| **Adress**  | Skogslyckegatan 86 Linköping                  |
| **Talar**   | Svenska, Engelska                             |

## Kort om mig

Värderar ni effektivitet, pålitlighet och kvalitet? Det gör jag med!
Jag är en ambitiös och datorintresserad ung man,
med ett öga för detaljer.
Jag värderar open-source, och jag använder linux på alla mina datorer.
De programmeringsspråk jag använder mest är
PureScript, Haskell, Rust, och Nix,
men jag kan även Python, JavaScript / TypeScript och Java.
På fritiden spenderar jag ofta tid på personliga programmeringsprojekt,
som till exempel min egna hemsida.

Jag är också intresserad av cyklar,
eftersom de tar mig ut i naturen och får mig att känna mig extra levande.
Jag har fyra cyklar, varav en jag byggt själv från cykeldelar jag fick av min far.

## Mina erfarenheter från arbetslivet

### \[2023-XXXX\]: Heltidsutvecklare hos Lesslie

### \[2022-2023\]: Sysadmin / IT-support i Microsoft-miljö på SundaHus

Vanliga arbetsuppgifter innefattade att sätta upp nya användarkonton
och datorer, och agera som intern IT-support.
Jag har även installerat och konfigurerat övervakning av deras servrar,
deras backup-server och deras nya UPS.

## Min utbildning

- **Civilingenjör i Mjukvaruteknik** på Linköpings Universitet. *(Ej avslutad)*
- **Teknikprogrammet** på Ullvigymnasiet i Köping, inriktning Datateknik.

## Mina projekt

### \[2023,2024\] LiTHe Kod Webmaster

LiTHe kod är en studentförening vid Linköpings universitet
vars syfte är att främja och inspirera intresset för programmering,
mjukvaruutveckling och allt som hör till.
Jag var ansvarig för LiTHe Kods hemsida under denna period

### \[2022\] Min hemsida

Att utveckla min egna hemsida har alltid varit något jag velat göra.
Så jag har gjort en, helt från grunden.

### \[2021\] D-LAN Webb & Stream

D-LAN är en projektgrupp på LiU som håller i en spelkväll varje år på campus.
Under läsåret 2020 - 2021 så ansvarade jag för deras hemsida.

### \[2020\] Kylbrants foton

Jag utvecklade en hemsida till Sandra Kylbrants verksamhet som amatörfotograf.
