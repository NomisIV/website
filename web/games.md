% Sick games
# Sick games

## [Minotaur Hunter](/games/minotaur-hunter/index.html)

![Screenshot of Minotaur Hunter gameplay](/assets/images/minotaur-hunter.png){width=90%}

- Explore the maze
- Kill all the monsters
- Survive (if you can)

### Controls

- Use WASD or arrow keys to move
- Use spacebar to shoot (you need ammo for that though)

### Backstory

You are the Minotaur Hunter. You enter the Minotaur's labyrinth to kill it for it's magical hide, but unfortunately you forgot your ammunition at home. Can you find the single shotgun shell placed somewhere in the maze? It is your only hope now.

This game was made at Spring Game Jam 2024 in Linköping, together with [Astrid Lauenstein](https://github.com/Astrid-Lauen), who did most of the coding. The sprites were purchased from [Deep Dive Game Studios](https://deepdivegamestudio.itch.io/)

