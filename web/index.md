% NomisIV

```{=html}
<div style="display: flex; gap: 2em; margin-top: 1em;">
  <img src="/assets/nomisiv.svg" style="width: 20ch; margin: 0;">
  <div>
    <h1 style="margin-bottom: 0;">Simon // NomisIV</h1>
  </div>
</div>
```

Hi there, I'm Simon!

Professionally I work with software development at a fintech startup called [Lesslie](https://lesslie.se/en).
Off work I'm often tinkering with my bikes or my computers - I really like making my own unique things.
I deeply value open-source, anti-capitalism, reusing and repurposing old stuff, natural materials, and things that do one thing and do it well.

Enough of that, now go on read the rest of this website!

## Here, have som links as a treat:

- [Contact me](contact)
- [My blog "No One Asked"](blog)
- [My recipes](recipes)
- [My projects](projects)
- [My lists](lists)
- [My games](games)

## Friends' websites

- [dethcrvsh.com](https://dethcrvsh.com/)
- [quaqqer.dev](https://quaqqer.dev)
- [A chip 8 emulator by Henry Andersson](https://henryandersson.github.io/chip8/)
- [lasersköld.se](https://lasersköld.se/home/blog/) (In swedish only)
