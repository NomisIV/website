% My Lists
# My Lists

Here are some assorted lists of assorted information. I hope you enjoy!

## Good movies

*(All links go to imdb.com)*

- [The LEGO Batman Movie](https://www.imdb.com/title/tt4116284) - This has absolutely no right being this good
- [2001: A Space Odyssey](https://www.imdb.com/title/tt0062622) - the grand daddy of space sci-fi (I think)
- [Beyond the Black Rainbow](https://www.imdb.com/title/tt1534085) - really weird but very aesthetic
- [The Matrix](https://www.imdb.com/title/tt0133093) - I can only wish to be as cool as Neo
- [Interstellar](https://www.imdb.com/title/tt0816692) - *This maneuver is gonna cost us 51 years!*
- [The Big Lebowski](https://www.imdb.com/title/tt0118715) - *He peed on my fucking rug*

## Movies that are so bad that they are good

*(All links go to imdb.com)*

- [The Room](https://www.imdb.com/title/tt0368226) - *I did not hit her!* Oh hi Mark
- [Deathrace](https://www.imdb.com/title/tt0452608) - this is mad max, but an arcade video game, but a movie
- [Cowboys vs Dinosaurs](https://www.imdb.com/title/tt3252786) - exactly what the title implies
- [Space Chimps 2](https://www.imdb.com/title/tt1537481) - everyone is fighting over a wii remote
- [Battleship](https://www.imdb.com/title/tt1440129) - "Yo, what if we made a movie about the game battleship" "But like with aliens and stuff"

## Cool corners of *the web*:

- [based.cooking](https://based.cooking/): A website with recipies. Nothing less, nothing more
- [landchad.net](https://landchad.net/): A website for setting up your own "cloud"
- [Complete PDF reference](https://www.adobe.com/content/dam/acom/en/devnet/pdf/pdfs/pdf_reference_archives/PDFReference.pdf): Not a website, but a PDF describing the Portable Document Format
- [findafriend.club](https://findafriend.club): This is what got me through the pandemic
- [bar.com](https://bar.com): A story about emails, and a bunch of bad jokes
- [motherfuckingwebsite.com](https://motherfuckingwebsite.com): The perfect website.
- [googolplexwrittenout.com](https://www.googolplexwrittenout.com/): Probably the longest book series yet
- [theworstwebsiteever.com](https://theworstwebsiteever.com): The worst website ever
- [floor796.com](https://floor796.com): An ever expanding animation of a space station
- [neal.fun](https://neal.fun): A collection of interesting web-based games
- [catb.org/~esr/jargon/html/M/metasyntactic-variable](http://catb.org/~esr/jargon/html/M/metasyntactic-variable.html): information about metasyntactic variables
- [pylonofthemonth.org](https://www.pylonofthemonth.org/): Self explanatory
- [beedogs.online](http://beedogs.online): Dogs in bee costumes

You can also check out the websites similar to mine [at my front page](/)
