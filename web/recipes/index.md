% My Recipes
# My Recipes

Some of these dishes are my own creations,
and some are just good recipes that I've come across.

When writing the recipes I try to make them quite general.
Usually I don't find the *perfect* recipe for what I have at home,
but instead I adapt an existing recipe for what I have at home.
This means that the measurements and ingredients are rarely exact requirements,
but more just there to convey a sense of the intended proportions and character.

## Recipes

{recipes}

Smaklig måltid! :)
