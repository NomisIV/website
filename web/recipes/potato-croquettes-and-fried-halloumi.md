% Potato Croquettes and Fried Halloumi
# Potato Croquettes and Fried Halloumi

*(~3 portions, 20 minutes)*

## Ingredients

- 1 bag of **Potato croquettes** *(~600g)*
- 1 block of **Halloumi** *(~250g)*
- 1 box of **Chickpeas** *(~380g)*
- 1 **Bell / snack pepper**

### Spices

- Salt
- Ground black pepper
- BBQ spice

## Instructions

1. Bake the potato croquettes
1. Cut the halloumi into pieces and fry on medium high heat
1. After a couple minutes, add the spices
1. Wait for the halloumi juice to boil away and the halloumi getting a golden surface
1. Add the vegetables and chickpeas and fry for ~10 more minutes
1. Bone apple teeth!

## Notes

- If the halloumi starts melting and sticking to itself, lower the heat.
- The bell pepper can be replaced with corn and carrot or other juicy and sweet veggies.
- Microwaved potato croquettes are not a hit, so don't bake too many.