% Potato Salad with Falafel
# Potato Salad with Falafel

*(~6 portions, 30 minutes)*

## Ingredients

- 1kg of potatoes
- 1 red onion
- 1/2 a leek
- 1 cucumber
- 1/2 a head of lettuce
- 2dl Crème Fraîche
- 5dl Turkish Yoghurt
- 2 tablespoons of olive oil *(~30ml)*
- 4 tablespoons of mustard *(~60ml)*

### Spices

- Salt
- Ground white pepper
- Dill
- Misc salad seasoning (whatever you feel like)

## Instructions

1. Prepare and boil the potatoes
1. Chop the vegetables while the potatoes are boiling
1. Add the vegetables, oil, mustard and all the spices in a bowl
1. When the potatoes are done, slice them and add them to the bowl
1. Mix thoroughly and serve with fresh falafel

## Notes

- The Crème Fraîche and Turkish Yoghurt can be replaced with around 5dl to 1l
  of any kind of cream or yoghurt you might have at home.
- I think white cheese could be a sucessfull addition to the salad
- If the potatoes are too hot to cut into pieces,
  try pouring cold water in the pot to cool them off
- Don't make extra falafel for leftovers. They don't microwave well