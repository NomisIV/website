% Veggie Burritos
# Veggie Burritos

*(~4 portions, 30 minutes)*

## Ingredients

- 8 **Tortilla wraps**
- 1 block of **Halloumi** *(~250g)*
- 1 box of **Kidney beans** *(~380g)*
- 1 **Onion**
- ~3 **Carrots**
- One or more of the following:
  - 1/2 **Cauliflower**
  - 1 **Zucchini**
  - 1 **Bell / snack pepper**
  - 1 can of **Corn**

### Spices

- Any taco / fajita spice mix
- Cumin
- Salt
- (Chili flakes / chili powder / paprika)

## Instructions

1. Prepare the vegetables and the halloumi
1. Fry the onion on high heat until soft
1. Add the rest of the vegetables, and a bit (like 1/2 dl) of water
1. Let the vegetables fry for ~10 minutes on high heat. Stir occasionally
1. Add the beans, halloumi and all of the spices. Now you can lower the heat
1. Let fry for another ~5 minutes
1. Serve the filling in the tortillas

## Notes

- I prefer shredding the carrots as opposed to cutting them into pieces.
- I also prefer chopping the onions in rather big pieces.
- I usually fry the filling in a large cast iron pot,
  but a larger frying pan works too.
- You can try this with whatever other veggies you have at home.
- Some people like heating their tortillas, but I don't personally