% Wishlist
# Wishlist

**NOTE:** I know some of these things aren't the most affordable gifts,
but they are things I genuinely want and will use.

**NOTE:** Some items on this list can be bought second-hand for less money.
As long as they are functionally sound, I don't mind :)

**NOTE:** This wishlist is not exhaustive,
that is to say, there are things I would appreciate that aren't on this list.

- A Casio A158W watch ([ebay link](https://www.ebay.com/sch/i.html?_nkw=casio+A158W))
- An 8-port network switch (preferably NETGEAR)
- A PCIe WiFi network card (to be used as an access point in my server)
- A 4-port M.2 NVMe SSD to PCIe adapter ([like this one](https://www.ebay.com/itm/204581037581))
- A really good kitchen knife
- Any album on my [bandcamp profile wishlist](https://bandcamp.com/nomisiv/wishlist)
- An [MNT Reform](https://shop.mntre.com/products/mnt-reform)
  or [MNT Pocket Reform](https://www.crowdsupply.com/mnt/pocket-reform)
- A [PinePhone Pro](https://pine64.com/product/pinephone-pro-explorer-edition/)
  or [PineNote](https://pine64.com/product/pinenote-developer-edition/)
  or [PineTab](https://pine64.com/product/pinetab-10-1-linux-tablet/)

Less tangible wishes:

- The obligatory *world peace*
- Infinite time to work on my open source projects
- The [Servo](https://servo.org) browser engine to be ready to use
- [Redox OS](https://www.redox-os.org/) to be ready to use
- The complete demise of Google, Microsoft and Meta
